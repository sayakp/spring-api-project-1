package com.epam.esm.exception;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handles {@link DatabaseException} which is thrown when a database error occurs.
     *
     * @param ex the exception
     * @return a ResponseEntity with a NOT FOUND status and a message indicating the issue.
     */
    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity<String> handleDatabaseException(DatabaseException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

    /**
     * Handles {@link ResponseStatusException} which is thrown when a specific HTTP status code is needed.
     *
     * @param ex the exception
     * @return a ResponseEntity with the status defined in the exception and a message indicating the issue.
     */
    @ExceptionHandler({ResponseStatusException.class})
    public ResponseEntity<String> handleResponseStatusException(ResponseStatusException ex) {
        return ResponseEntity.status(ex.getStatus()).body(ex.getMessage());
    }

    /**
     * Handles {@link EmptyResultDataAccessException}, which is thrown when a result was expected to have at least one
     * row (or element) but zero rows (or elements) were actually returned.
     *
     * @param ex the exception
     * @return a ResponseEntity with a NOT FOUND status and a message indicating that the item was not found.
     */
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<String> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Item not found, " + ex.getMessage());
    }

    /**
     * Handles {@link DataAccessException}, which is the super class for all exceptions thrown in the case of any
     * problem with data access.
     *
     * @param ex the exception
     * @return a ResponseEntity with an INTERNAL SERVER ERROR status and a message indicating that a database error
     * occurred.
     */
    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<String> DataAccessException(DataAccessException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Database error occurred. " +
                "Please try again later. " + ex.getMessage());
    }

    /**
     * Handles {@link IllegalArgumentException}, which is thrown to indicate that a method has been passed an illegal
     * or inappropriate argument.
     *
     * @param ex the exception
     * @return a ResponseEntity with a BAD REQUEST status and a message indicating that an invalid argument was passed.
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid argument");
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Malformed JSON request");
    }

    /**
     * Handles {@link Exception}, a general exception, which is thrown when an unexpected error occurs.
     *
     * @param ex the exception
     * @return a ResponseEntity with an INTERNAL SERVER ERROR status and a general error message.
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred");
    }


}