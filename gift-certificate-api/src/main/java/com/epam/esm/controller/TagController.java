package com.epam.esm.controller;

import com.epam.esm.model.Tag;
import com.epam.esm.service.CrudService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {
    CrudService<Tag> tagService;

    /**
     * Constructs a new controller with the specified TagService.
     *
     * @param tagService the service to use for Tag-related operations
     */
    public TagController(CrudService<Tag> tagService) {
        this.tagService = tagService;
    }

    /**
     * Retrieves all tags.
     *
     * @return a ResponseEntity containing a list of all tags
     */
    @GetMapping()
    public ResponseEntity<List<Tag>> getAll() {
        return ResponseEntity.ok(tagService.findAll());
    }

    /**
     * Retrieves the tag with the specified ID.
     *
     * @param id the ID of the tag to retrieve
     * @return a ResponseEntity containing the tag
     */
    @GetMapping("/{id}")
    public ResponseEntity<Tag> getTag(@PathVariable Long id) {
        return ResponseEntity.ok(tagService.findById(id));
    }

    /**
     * Creates a new tag with the details specified in the request body.
     *
     * @param tag the details of the tag to create
     * @return a ResponseEntity containing the created tag
     */
    @PostMapping()
    public ResponseEntity<Tag> createTag(@RequestBody Tag tag) {
        Tag createdTag = tagService.save(tag);
        return ResponseEntity.ok(createdTag);
    }

    /**
     * Deletes the tag with the specified ID.
     *
     * @param id the ID of the tag to delete
     * @return a ResponseEntity indicating the completion of the operation
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTag(@PathVariable Long id) {
        tagService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
