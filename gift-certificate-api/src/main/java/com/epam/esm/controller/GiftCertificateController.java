package com.epam.esm.controller;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.service.GiftCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gift-certificate")
public class GiftCertificateController {
    GiftCertificateService<GiftCertificate> giftCertificateService;

    /**
     * Constructs a new controller with the specified GiftCertificateService.
     *
     * @param giftCertificateService the service to use for GiftCertificate-related operations
     */
    @Autowired
    public GiftCertificateController(GiftCertificateService<GiftCertificate> giftCertificateService) {
        this.giftCertificateService = giftCertificateService;
    }

    /**
     * Retrieves all gift certificates.
     *
     * @return a list of all gift certificates
     */
    @GetMapping()
    public ResponseEntity<List<GiftCertificate>> getAll() {
        return ResponseEntity.ok(giftCertificateService.findAll());
    }

    /**
     * The function allows for searching for gift certificates with a specific tag name and text, and sorting the results.
     * The results are sorted by the specified field and direction. The function handles null values for all parameters.
     *
     * @param tagName The name of the tag to filter by. This is an optional request parameter.
     *                If null or not provided, the function does not filter by tag name.
     * @param searchText The text to search for in the name and description of the gift certificates.
     *                   This is an optional request parameter. If null or not provided, the function does not perform a search.
     * @param sortBy The field to sort the results by. This is an optional request parameter with a default value of 'name'.
     * @param order The direction to sort the results in. This is an optional request parameter with a default value of 'asc'.
     *              Only 'asc' (ascending) or 'desc' (descending) are allowed.
     * @return A ResponseEntity containing a list of gift certificates matching the provided search criteria and sorted as specified,
     *         and an HTTP status of 200 (OK).
     */
    @GetMapping("/certificates")
    public ResponseEntity<List<GiftCertificate>> getCertificates(
            @RequestParam(required = false) String tagName,
            @RequestParam(required = false) String searchText,
            @RequestParam(defaultValue = "name") String sortBy,
            @RequestParam(defaultValue = "asc") String order) {
        List<GiftCertificate> certificates = giftCertificateService.getSortedCertificates(tagName, searchText, sortBy, order);
        return ResponseEntity.ok(certificates);
    }

    /**
     * Retrieves the gift certificate with the specified ID.
     *
     * @param id the ID of the gift certificate to retrieve
     * @return a ResponseEntity containing the gift certificate
     */
    @GetMapping("/{id}")
    public ResponseEntity<GiftCertificate> getGiftCertificate(@PathVariable Long id) {
        return ResponseEntity.ok(giftCertificateService.findById(id));
    }

    /**
     * Creates a new gift certificate with the details specified in the request body.
     *
     * @param giftCertificate the details of the gift certificate to create
     * @return a ResponseEntity containing the created gift certificate
     */
    @PostMapping()
    public ResponseEntity<GiftCertificate> createGiftCertificate(@RequestBody GiftCertificate giftCertificate) {
        return ResponseEntity.ok(giftCertificateService.save(giftCertificate));
    }


    /**
     * Deletes the gift certificate with the specified ID.
     *
     * @param id the ID of the gift certificate to delete
     * @return a ResponseEntity indicating the completion of the operation
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGiftCertificate(@PathVariable Long id) {
        giftCertificateService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Updates the gift certificate with the details specified in the request body.
     *
     * @param giftCertificate the new details of the gift certificate
     * @return a ResponseEntity containing the updated gift certificate
     */
    @PutMapping("/{id}")
    public ResponseEntity<GiftCertificate> updateGiftCertificate(@RequestBody GiftCertificate giftCertificate, @PathVariable Long id) {
        giftCertificate.setId(id);
        return ResponseEntity.ok(giftCertificateService.update(giftCertificate));
    }

}
