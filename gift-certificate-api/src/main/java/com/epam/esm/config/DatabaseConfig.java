package com.epam.esm.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {
    @Bean
    public DataSource dataSource(@Value("${spring.datasource.url}") String datasourceUrl,
                                 @Value("${spring.datasource.username}") String datasourceUsername,
                                 @Value("${spring.datasource.password}") String datasourcePassword,
                                 @Value("${spring.datasource.driver-class-name}") String datasourceDriverClassName,
                                 @Value("${spring.datasource.hikari.maximum-pool-size}") int datasourceMaximumPoolSize,
                                 @Value("${spring.datasource.hikari.idle-timeout}") Long datasourceIdleTimeout) {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(datasourceUrl);
        dataSource.setUsername(datasourceUsername);
        dataSource.setPassword(datasourcePassword);
        dataSource.setDriverClassName(datasourceDriverClassName);
        dataSource.setIdleTimeout(datasourceIdleTimeout);
        dataSource.setMaximumPoolSize(datasourceMaximumPoolSize);

        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}

