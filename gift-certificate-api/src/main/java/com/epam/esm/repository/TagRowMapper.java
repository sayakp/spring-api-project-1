package com.epam.esm.repository;

import com.epam.esm.model.Tag;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class is a custom RowMapper implementation to map a row of the result set to a Tag object.
 * <p>
 * The mapRow method is overridden to define how the mapping is done.
 */
public class TagRowMapper implements RowMapper<Tag> {

    /**
     * Maps a row from the result set to a Tag object.
     *
     * @param resultSet the ResultSet object, which contains data returned from the SQL query
     * @param i         the number of the current row. Note that this parameter is not used here
     * @return a Tag object populated with the data from the current row of the ResultSet
     * @throws SQLException in case of SQL errors
     */
    @Override
    public Tag mapRow(ResultSet resultSet, int i) throws SQLException {
        Tag output = new Tag();
        output.setId(resultSet.getLong("id"));
        output.setName(resultSet.getString("name"));
        return output;
    }
}
