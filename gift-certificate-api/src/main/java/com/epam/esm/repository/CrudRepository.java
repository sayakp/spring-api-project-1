package com.epam.esm.repository;

import java.util.List;

public interface CrudRepository<T> {


    List<T> findAll();

    T findById(Long id);

    T save(T entity);

    T update(T entity);

    void deleteById(Long id);

}
