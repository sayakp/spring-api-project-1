package com.epam.esm.repository;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class GiftCertificateRepositoryImpl implements GiftCertificateRepository<GiftCertificate> {

    private final JdbcTemplate jdbcTemplate;
    private final TagRepositoryImpl tagRepository;

    /**
     * Constructor for the GiftCertificateRepository.
     *
     * @param jdbcTemplate  used for interacting with the database
     * @param tagRepository the repository to manage Tag-related operations
     */
    public GiftCertificateRepositoryImpl(JdbcTemplate jdbcTemplate, TagRepositoryImpl tagRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.tagRepository = tagRepository;
    }

    /**
     * Retrieves all GiftCertificates from the database.
     *
     * @return a List containing all GiftCertificates
     */
    @Override
    public List<GiftCertificate> findAll() {
        String query = "SELECT * FROM gift_certificate";
        List<GiftCertificate> giftCertificates = jdbcTemplate.query(query, new GiftCertificateRowMapper());
        giftCertificates.forEach(giftCertificate ->
                giftCertificate.setTags(tagRepository.findByGiftCertificateId(giftCertificate.getId())));
        return giftCertificates;
    }

    /**
     * Finds a GiftCertificate by id.
     *
     * @param id the id of the GiftCertificate
     * @return the found GiftCertificate
     * @throws DatabaseException if no GiftCertificate is found with the provided id
     */
    @Override
    public GiftCertificate findById(Long id) {
        try {
            String query = "SELECT * FROM gift_certificate WHERE id = ?";
            GiftCertificate giftCertificate = jdbcTemplate.queryForObject(query, new GiftCertificateRowMapper(), id);
            giftCertificate.setTags(tagRepository.findByGiftCertificateId(id));
            return giftCertificate;
        } catch (DataAccessException exception) {
            throw new DatabaseException("Item not found with id=" + id);
        }
    }

    /**
     * Saves a new GiftCertificate in the database.
     *
     * @param giftCertificate the GiftCertificate to save
     * @return the saved GiftCertificate with the generated id
     */
    @Override
    public GiftCertificate save(GiftCertificate giftCertificate) {
        String query = "INSERT INTO gift_certificate (name, description, price, duration ,create_date, last_update_date)" +
                "VALUES (?, ?, ?, ?, ?, ?)";


        KeyHolder keyHolder = new GeneratedKeyHolder();
        Instant currentDate = Instant.now();

        int newRows = jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, new String[]{"id"});
            ps.setString(1, giftCertificate.getName());
            ps.setString(2, giftCertificate.getDescription());
            ps.setBigDecimal(3, giftCertificate.getPrice());
            ps.setInt(4, giftCertificate.getDuration());
            ps.setTimestamp(5, Timestamp.from(currentDate));
            ps.setTimestamp(6, Timestamp.from(currentDate));
            return ps;
        }, keyHolder);

        long newEntryPk = keyHolder.getKey().longValue();

        List<Tag> updatedTags = updateGiftCertificateTags(giftCertificate.getTags(), newEntryPk);
        giftCertificate.setId(newEntryPk);
        giftCertificate.setTags(updatedTags);
        giftCertificate.setCreateDate(Date.from(currentDate));
        giftCertificate.setLastUpdateDate(Date.from(currentDate));
        return giftCertificate;
    }

    /**
     * Retrieves a list of gift certificates from the database based on the provided parameters.
     * The function allows for searching for gift certificates with a specific tag name and text, and sorting the results.
     * The results are sorted by the specified field and direction. The function handles null values for all parameters.
     *
     * @param tagName The name of the tag to filter by. If null, the function does not filter by tag name.
     * @param searchText The text to search for in the name and description of the gift certificates.
     *                  If null, the function does not perform a search.
     * @param sortBy The field to sort the results by. If null, the function sorts by the gift certificate name.
     * @param sortDirection The direction to sort the results in. If null, the function sorts in ascending order.
     *                      Only 'asc' (ascending) or 'desc' (descending) are allowed.
     * @return A list of gift certificates matching the provided search criteria and sorted as specified.
     * @throws IllegalArgumentException if sortBy contains characters other than alphanumeric and underscores,
     *                                  or if sortDirection is neither 'asc' nor 'desc'.
     */
    public List<GiftCertificate> getSortedCertificates(
            String tagName,
            String searchText,
            String sortBy,
            String sortDirection) {
        StringBuilder query = new StringBuilder(
                "SELECT gc.* FROM gift_certificate gc ");

        List<Object> params = new ArrayList<>();

        // Tag filter
        if (tagName != null) {
            query.append("JOIN gift_certificate_tag gct ON gct.gift_certificate_id = gc.id ");
            query.append("JOIN tag t ON t.id = gct.tag_id AND t.name = ? ");
            params.add(tagName);
        }

        // Search filter
        if (searchText != null) {
            if (params.isEmpty()) {
                query.append("WHERE ");
            } else {
                query.append("AND ");
            }
            query.append("(gc.name LIKE ? OR gc.description LIKE ?) ");
            params.add("%" + searchText + "%");
            params.add("%" + searchText + "%");
        }

        // Sort
        if (sortBy != null) {
            query.append("ORDER BY gc.").append(sortBy).append(" ");
            if (sortDirection != null) {
                query.append(sortDirection);
            }
        }


        List<GiftCertificate> giftCertificatesOutput = jdbcTemplate.query(query.toString(),
                new GiftCertificateRowMapper(),
                params.toArray());
        giftCertificatesOutput.forEach(giftCertificate -> giftCertificate
                .setTags(tagRepository.findByGiftCertificateId(giftCertificate.getId())));

        return giftCertificatesOutput;
    }

    /**
     * Updates an existing GiftCertificate in the database.
     *
     * @param giftCertificate the GiftCertificate with updated data
     * @return the updated GiftCertificate
     */
    public GiftCertificate update(GiftCertificate giftCertificate) {
        String query = "UPDATE gift_certificate SET name = ?, description = ?, price = ?, duration = ?, " +
                "create_date = ?, last_update_date = ? WHERE id = ?";

        jdbcTemplate.update(query,
                giftCertificate.getName(),
                giftCertificate.getDescription(),
                giftCertificate.getPrice(),
                giftCertificate.getDuration(),
                giftCertificate.getCreateDate(),
                giftCertificate.getLastUpdateDate(),
                giftCertificate.getId());

        // Update tags if we receive a new tags list, otherwise get leave the tags alone
        if (giftCertificate.getTags() != null) {
            String deleteQuery = "DELETE FROM gift_certificate_tag WHERE gift_certificate_id = ?";
            jdbcTemplate.update(deleteQuery, giftCertificate.getId());
            List<Tag> updatedTags = updateGiftCertificateTags(giftCertificate.getTags(), giftCertificate.getId());
            giftCertificate.setTags(updatedTags);
        } else {
            giftCertificate.setTags(tagRepository.findByGiftCertificateId(giftCertificate.getId()));
        }
        return giftCertificate;
    }


    /**
     * Helper method to update the Tags of a given GiftCertificate.
     *
     * @param tags              list of Tags to associate with the GiftCertificate
     * @param giftCertificateId the id of the GiftCertificate
     * @return list of Tags that have been associated with the GiftCertificate
     */
    public List<Tag> updateGiftCertificateTags(List<Tag> tags, Long giftCertificateId) {
        try {
            String insertQuery = "INSERT INTO gift_certificate_tag (gift_certificate_id, tag_id) VALUES (?, ?)";
            for (int i = 0; i < tags.size(); i++) {
                Tag tag = tags.get(i);
                // If a tag id is not given, we have to look if the tag name is already in the database
                if (tag.getId() == null) {
                    Tag existingTag = tagRepository.findByName(tag.getName());
                    if (existingTag != null) {
                        tag = existingTag;
                    } else {
                        tag = tagRepository.save(tag);
                    }
                    tags.set(i, tag); // Update the list with the new Tag
                }
                jdbcTemplate.update(insertQuery, giftCertificateId, tag.getId());
            }
        } catch (DataIntegrityViolationException exception) {
            throw new DatabaseException("Item with id " + giftCertificateId + " does not exist");
        }
        return tags;
    }

    /**
     * Deletes a GiftCertificate by id.
     *
     * @param id the id of the GiftCertificate to delete
     * @throws DatabaseException if no GiftCertificate is found with the provided id
     */
    @Override
    public void deleteById(Long id) {
        String deleteManyToManyRelationshipQuery = "DELETE FROM gift_certificate_tag WHERE gift_certificate_id = ?";
        jdbcTemplate.update(deleteManyToManyRelationshipQuery, id);

        String query = "DELETE FROM gift_certificate WHERE id= ?";
        int affectedRows = jdbcTemplate.update(query, id);
        if (affectedRows == 0) {
            throw new DatabaseException("No entry to delete, item id=" + id);
        }
    }
}
