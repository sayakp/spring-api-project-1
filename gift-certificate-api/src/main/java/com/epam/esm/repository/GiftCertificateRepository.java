package com.epam.esm.repository;

import java.util.List;

public interface GiftCertificateRepository<T> extends CrudRepository<T>{
    List<T> getSortedCertificates(
            String tagName,
            String searchText,
            String sortBy,
            String sortDirection);
}
