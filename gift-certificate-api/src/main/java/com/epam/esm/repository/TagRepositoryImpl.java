package com.epam.esm.repository;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.Tag;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class TagRepositoryImpl implements CrudRepository<Tag> {

    private JdbcTemplate jdbcTemplate;

    /**
     * Constructs a new repository with the specified JdbcTemplate.
     *
     * @param jdbcTemplate the JdbcTemplate to perform database operations
     */
    public TagRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    /**
     * Retrieves all tags.
     *
     * @return a list of all tags
     */
    public List<Tag> findAll() {
        String query = "SELECT * FROM tag";
        return jdbcTemplate.query(query, new TagRowMapper());
    }


    /**
     * Retrieves the tag with the specified ID.
     *
     * @param id the ID of the tag to retrieve
     * @return the tag with the given ID
     * @throws DatabaseException if no tag is found with the given ID
     */
    public Tag findById(Long id) {
        try {
            String query = "SELECT * FROM tag WHERE id = ?";
            return jdbcTemplate.queryForObject(query, new TagRowMapper(), id);
        } catch (DataAccessException exception) {
            throw new DatabaseException("Item not found with id=" + id);
        }
    }

    /**
     * Retrieves the tag with the specified name.
     *
     * @param name the name of the tag to retrieve
     * @return the tag with the given name, or null if no such tag exists
     */
    public Tag findByName(String name) {
        String query = "SELECT * FROM tag WHERE name=? LIMIT 1";
        try {
            return jdbcTemplate.queryForObject(query, new TagRowMapper(), name);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }


    /**
     * Inserts a new tag into the database.
     *
     * @param tag the tag to save
     * @return the saved tag, including its generated ID
     */
    public Tag save(Tag tag) {
        String query = "INSERT INTO tag (name) VALUES (?)";

        // Keyholder is being used so we can extract the created Tag id from the query
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(query, new String[]{"id"});
                    ps.setString(1, tag.getName());
                    return ps;
                },
                keyHolder);

        Long newTagId = keyHolder.getKey().longValue();
        tag.setId(newTagId);
        return tag;
    }

    /**
     * Updates the details of an existing tag in the database.
     *
     * @param tag the tag to update
     * @return the updated tag
     * @throws DatabaseException if no tag is found with the given ID
     */
    public Tag update(Tag tag) {
        String query = "UPDATE tag SET name = ? WHERE id = ?";
        int affectedRows = jdbcTemplate.update(query, tag.getName(), tag.getId());
        if (affectedRows == 0) {
            throw new DatabaseException("No entries updated. Tag id=" + tag.getId());
        }
        return tag;
    }

    /**
     * Deletes the tag with the specified ID.
     *
     * @param id the ID of the tag to delete
     * @throws DatabaseException if no tag is found with the given ID
     */
    public void deleteById(Long id) {
        String deleteManyToManyRelationshipQuery = "DELETE FROM gift_certificate_tag WHERE tag_id = ?";
        jdbcTemplate.update(deleteManyToManyRelationshipQuery, id);

        String deleteTagQuery = "DELETE FROM tag WHERE id= ?";
        int affectedRows = jdbcTemplate.update(deleteTagQuery, id);
        if (affectedRows == 0) {
            throw new DatabaseException("No entry to delete, item id=" + id);
        }
    }

    /**
     * Retrieves all tags associated with a specific gift certificate.
     *
     * @param id the ID of the gift certificate whose tags are to be retrieved
     * @return a list of tags associated with the gift certificate
     */
    public List<Tag> findByGiftCertificateId(Long id) {
        String query = "SELECT t.id, t.name FROM tag t " +
                "JOIN gift_certificate_tag gct ON gct.tag_id = t.id " +
                "WHERE gct.gift_certificate_id = ?";
        return jdbcTemplate.query(query, new TagRowMapper(), id);
    }
}
