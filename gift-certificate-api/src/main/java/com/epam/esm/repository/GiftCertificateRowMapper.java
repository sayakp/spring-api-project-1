package com.epam.esm.repository;

import com.epam.esm.model.GiftCertificate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * This class maps a row from the result of a database query to a GiftCertificate object.
 */
public class GiftCertificateRowMapper implements RowMapper<GiftCertificate> {

    /**
     * Maps the current row in the given ResultSet to a GiftCertificate object.
     * The ResultSet should come from a query on the "gift_certificate" table.
     *
     * @param resultSet the ResultSet to map
     * @param i         the number of the current row
     * @return a GiftCertificate object containing the data from the current row
     * @throws SQLException if a database access error occurs
     */
    @Override
    public GiftCertificate mapRow(ResultSet resultSet, int i) throws SQLException {
        GiftCertificate output = new GiftCertificate();
        output.setId(resultSet.getLong("id"));
        output.setName(resultSet.getString("name"));
        output.setDescription(resultSet.getString("description"));
        output.setPrice(resultSet.getBigDecimal("price"));
        output.setDuration(resultSet.getInt("duration"));
        output.setCreateDate(resultSet.getTimestamp("create_date"));
        output.setLastUpdateDate(resultSet.getTimestamp("last_update_date"));
        return output;
    }
}
