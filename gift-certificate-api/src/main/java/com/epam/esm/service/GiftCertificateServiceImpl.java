package com.epam.esm.service;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.repository.GiftCertificateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class GiftCertificateServiceImpl implements GiftCertificateService<GiftCertificate> {
    private final GiftCertificateRepository<GiftCertificate> giftCertificateRepository;


    /**
     * Constructs a new GiftCertificateService and autowires the GiftCertificateRepository.
     *
     * @param giftCertificateRepository the repository that this service will use for database interactions
     */
    @Autowired
    public GiftCertificateServiceImpl(GiftCertificateRepository<GiftCertificate> giftCertificateRepository) {
        this.giftCertificateRepository = giftCertificateRepository;
    }

    /**
     * Retrieves all gift certificates from the database.
     *
     * @return a list of all gift certificates
     */
    public List<GiftCertificate> findAll() {
        return giftCertificateRepository.findAll();
    }

    /**
     * Saves a new gift certificate to the database.
     * Rolls back the transaction if any exception occurs during the process.
     *
     * @param giftCertificate the gift certificate to be saved
     * @return the saved gift certificate
     * @throws ResponseStatusException if the gift certificate is missing required information
     */
    @Transactional(rollbackFor = Exception.class)
    public GiftCertificate save(GiftCertificate giftCertificate) {
        if (giftCertificateMissingInfo(giftCertificate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing info to create a new item");
        }
        return giftCertificateRepository.save(giftCertificate);
    }

    /**
     * Fetches a list of GiftCertificate entities from the repository. The retrieval of gift certificates can be controlled
     * by specifying certain parameters such as tagName, searchText, sortBy, and order.
     *
     * @param tagName     The name of the tag to filter the gift certificates by. If null, no tag filter is applied.
     * @param searchText  The text to search within the name and description of the gift certificates. If null, no text search is applied.
     * @param sortBy      The property of the gift certificate to sort the results by. This value should only contain alphanumeric characters and underscores.
     *                    If null, no specific sorting is applied.
     * @param order       The direction to sort the results in. This value should be either 'asc' or 'desc'.
     *                    If null, no specific sorting direction is applied.
     * @return            A List of GiftCertificate entities that match the provided search criteria, sorted as specified.
     * @throws IllegalArgumentException if sortBy contains characters other than alphanumeric and underscores,
     *                                  or if order is neither 'asc' nor 'desc'.
     */
    @Transactional(readOnly = true)
    public List<GiftCertificate> getSortedCertificates(String tagName, String searchText, String sortBy, String order) {
        // Check if sortBy contains only alphanumeric characters and underscores
        if (sortBy != null && !sortBy.matches("^[a-zA-Z0-9_]*$")) {
            throw new IllegalArgumentException("sortBy can only contain alphanumeric characters and underscores");
        }
        // Check if order is "asc" or "desc"
        if (order != null && !order.equalsIgnoreCase("asc") && !order.equalsIgnoreCase("desc")) {
            throw new IllegalArgumentException("order can only be 'asc' or 'desc'");
        }
        return giftCertificateRepository.getSortedCertificates(tagName, searchText, sortBy, order);
    }

    /**
     * Retrieves a gift certificate by its id.
     *
     * @param id the id of the gift certificate
     * @return the gift certificate
     * @throws ResponseStatusException if there is no gift certificate with the given id
     */
    public GiftCertificate findById(Long id) {
        try {
            return giftCertificateRepository.findById(id);
        } catch (DatabaseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Gift Certificate with id " + id + " not found");
        }
    }

    /**
     * Updates an existing gift certificate in the database.
     * Rolls back the transaction if any exception occurs during the process.
     *
     * @param giftCertificate the gift certificate with updated information
     * @return the updated gift certificate
     * @throws ResponseStatusException if the gift certificate is missing required information
     */
    @Transactional(rollbackFor = Exception.class)
    public GiftCertificate update(GiftCertificate giftCertificate) {
        if (giftCertificateIsEmpty(giftCertificate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No update arguments provided");
        } else if (giftCertificate.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No item id provided");
        }
        // Fetch the existing giftCertificate from the database
        GiftCertificate existingGiftCertificate = giftCertificateRepository.findById(giftCertificate.getId());

        // Check each field of updatedGiftCertificate, if it's not null, put it to the map
        if (giftCertificate.getName() != null) {
            existingGiftCertificate.setName(giftCertificate.getName());
        }
        if (giftCertificate.getDescription() != null) {
            existingGiftCertificate.setDescription(giftCertificate.getDescription());
        }
        if (giftCertificate.getPrice() != null) {
            existingGiftCertificate.setPrice(giftCertificate.getPrice());
        }
        if (giftCertificate.getDuration() != null) {
            existingGiftCertificate.setDuration(giftCertificate.getDuration());
        }
        if (giftCertificate.getTags() != null) {
            existingGiftCertificate.setTags(giftCertificate.getTags());
        } else {
            existingGiftCertificate.setTags(null);
        }
        existingGiftCertificate.setLastUpdateDate(Date.from(Instant.now()));

        return giftCertificateRepository.update(existingGiftCertificate);
    }

    /**
     * Deletes a gift certificate by its id.
     *
     * @param id the id of the gift certificate
     * @throws ResponseStatusException if there is no gift certificate with the given id
     */
    public void deleteById(Long id) {
        try {
            giftCertificateRepository.deleteById(id);
        } catch (DatabaseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Gift Certificate with id " + id + " not found");
        }
    }


    /**
     * Checks if ALL fields in the GiftCertificate are null. If true, it means that there is no data provided to update
     * the GiftCertificate, which can be used as an early validation step before attempting to perform an update operation.
     *
     * @param giftCertificate - The GiftCertificate object to check.
     * @return - A boolean value. Returns 'true' if all fields in the GiftCertificate object are null, 'false' otherwise.
     */
    private boolean giftCertificateIsEmpty(GiftCertificate giftCertificate) {
        return giftCertificate.getName() == null &&
                giftCertificate.getTags() == null &&
                giftCertificate.getDuration() == null &&
                giftCertificate.getPrice() == null &&
                giftCertificate.getDescription() == null;
    }


    /**
     * Checks if any of the necessary fields in the GiftCertificate are missing (i.e., are null). These fields are
     * required to successfully create a new GiftCertificate entry in the database. If any are missing, it means the
     * provided GiftCertificate object is incomplete, and a new entry cannot be created.
     *
     * @param giftCertificate - The GiftCertificate object to check.
     * @return - A boolean value. Returns 'true' if any of the necessary fields in the GiftCertificate object are null,
     * 'false' otherwise.
     */
    private boolean giftCertificateMissingInfo(GiftCertificate giftCertificate) {
        return giftCertificate.getName() == null ||
                giftCertificate.getTags() == null ||
                giftCertificate.getDuration() == null ||
                giftCertificate.getPrice() == null ||
                giftCertificate.getDescription() == null;
    }
}
