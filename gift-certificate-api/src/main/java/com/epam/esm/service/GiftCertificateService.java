package com.epam.esm.service;

import java.util.List;

public interface GiftCertificateService<T> extends CrudService<T>{
    List<T> getSortedCertificates(String tagName, String searchText, String sortBy, String order);
}
