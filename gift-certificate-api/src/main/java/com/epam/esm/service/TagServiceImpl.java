package com.epam.esm.service;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class TagServiceImpl implements CrudService<Tag> {
    private final CrudRepository<Tag> tagRepository;

    /**
     * Constructs a new instance of TagService.
     *
     * @param tagRepository the repository to be used for data access operations.
     */
    public TagServiceImpl(CrudRepository<Tag> tagRepository) {
        this.tagRepository = tagRepository;
    }


    /**
     * Retrieves all existing tags from the data store.
     *
     * @return a list of Tag objects.
     */
    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    /**
     * Saves a new tag to the data store. If the name of the tag is not provided, a BAD REQUEST status is returned.
     *
     * @param tag the Tag object to be saved.
     * @return the saved Tag object.
     * @throws ResponseStatusException with status BAD REQUEST if the name of the tag is null.
     */
    @Override
    public Tag save(Tag tag) {
        if (tag.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing tag name");
        }
        return tagRepository.save(tag);
    }


    /**
     * Retrieves a tag with the specified id from the data store. If no such tag is found, a NOT FOUND status is returned.
     *
     * @param id the id of the tag to retrieve.
     * @return the retrieved Tag object.
     * @throws ResponseStatusException with status NOT FOUND if no tag with the specified id is found.
     */
    public Tag findById(Long id) {
        try {
            return tagRepository.findById(id);
        } catch (DatabaseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Tag with id " + id + " not found");
        }
    }

    /**
     * Method for updating a tag. This method is currently not implemented as updates for tags are not required.
     *
     * @param tag the Tag object to be updated.
     * @return null.
     */
    public Tag update(Tag tag) {
        return null;
    }

    /**
     * Deletes a tag with the specified id from the data store. If no such tag is found, a NOT FOUND status is returned.
     *
     * @param id the id of the tag to be deleted.
     * @throws ResponseStatusException with status NOT FOUND if no tag with the specified id is found.
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(Long id) {
        try {
            tagRepository.deleteById(id);
        } catch (DatabaseException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Tag with id " + id + " not found");
        }
    }
}
