package com.epam.esm;

import com.epam.esm.config.WebConfig;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class App {
    public static void main(String[] args) {
        try {
            int port = Integer.parseInt(System.getProperty("SERVER_PORT", "8080"));
            Server server = new Server(port);

            ServletContextHandler context = new ServletContextHandler();
            context.setContextPath("/");

            AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
            webApplicationContext.register(WebConfig.class);

            DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplicationContext);
            ServletHolder servletHolder = new ServletHolder(dispatcherServlet);
            context.addServlet(servletHolder, "/*");

            context.addServlet(DefaultServlet.class, "/");

            server.setHandler(context);
            server.start();
            server.join();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
