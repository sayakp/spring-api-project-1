package com.epam.esm.service;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.GiftCertificateRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class GiftCertificateServiceTest {
    @Mock
    private GiftCertificateRepositoryImpl giftCertificateRepository;

    @InjectMocks
    private GiftCertificateServiceImpl giftCertificateService;

    private GiftCertificate completeGiftCertificate1;
    private GiftCertificate incompleteGiftCertificate;
    private GiftCertificate noIdGiftCertificate;
    private GiftCertificate idOnlyGiftCertificate;
    private List<GiftCertificate> giftCertificates;
    private Tag tag1;
    private Tag tag2;
    private List<Tag> tagList1;
    private Date creationDate;

    @BeforeEach
    public void setUp() {
        tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Tag 1");

        tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Tag 2");

        tagList1 = new ArrayList<>();
        tagList1.add(tag1);


        creationDate = new Date();

        completeGiftCertificate1 = new GiftCertificate();
        completeGiftCertificate1.setId(1L);
        completeGiftCertificate1.setName("Test Certificate 1");
        completeGiftCertificate1.setDescription("This is a test certificate 1");
        completeGiftCertificate1.setPrice(new BigDecimal("50.00"));
        completeGiftCertificate1.setDuration(30);
        completeGiftCertificate1.setCreateDate(creationDate);
        completeGiftCertificate1.setLastUpdateDate(creationDate);
        completeGiftCertificate1.setTags(tagList1);

        // Gift Certificate with no Price and Duration set
        incompleteGiftCertificate = new GiftCertificate();
        incompleteGiftCertificate.setId(2L);
        incompleteGiftCertificate.setName("Incomplete Gift Cert");
        incompleteGiftCertificate.setDescription("This is a gift certificate with insufficient info");
        incompleteGiftCertificate.setCreateDate(creationDate);
        incompleteGiftCertificate.setLastUpdateDate(creationDate);
        incompleteGiftCertificate.setTags(tagList1);

        noIdGiftCertificate = new GiftCertificate();
        noIdGiftCertificate.setName("No id cert");
        noIdGiftCertificate.setDescription("This is a test certificate with a missing id");
        noIdGiftCertificate.setPrice(new BigDecimal("75.00"));
        noIdGiftCertificate.setDuration(45);
        noIdGiftCertificate.setCreateDate(creationDate);
        noIdGiftCertificate.setLastUpdateDate(creationDate);
        noIdGiftCertificate.setTags(tagList1);

        idOnlyGiftCertificate = new GiftCertificate();
        idOnlyGiftCertificate.setId(1L);

        giftCertificates = new ArrayList<>();
        giftCertificates.add(completeGiftCertificate1);
        giftCertificates.add(completeGiftCertificate1);
    }

    @Test
    void testFindALlValidGift() {
        Mockito.when(giftCertificateRepository.findAll()).thenReturn(giftCertificates);

        List<GiftCertificate> result = giftCertificateService.findAll();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(2, result.size());
    }

    @Test
    void testFindAllEmpty() {
        Mockito.when(giftCertificateRepository.findAll()).thenReturn(new ArrayList<>());

        List<GiftCertificate> result = giftCertificateService.findAll();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void testFindByIdValidGift() {
        GiftCertificate giftCertificate = completeGiftCertificate1;
        Mockito.when(giftCertificateRepository.findById(giftCertificate.getId())).thenReturn(giftCertificate);

        GiftCertificate result = giftCertificateService.findById(giftCertificate.getId());

        Assertions.assertEquals(1L, result.getId());
        Assertions.assertEquals(30, result.getDuration());
        Assertions.assertEquals("Test Certificate 1", result.getName());
        Assertions.assertEquals(new BigDecimal("50.00"), result.getPrice());
        Assertions.assertEquals(creationDate, result.getCreateDate());
        Assertions.assertEquals(creationDate, result.getLastUpdateDate());
        Assertions.assertEquals(tagList1.size(), result.getTags().size());
    }

    @Test
    void testFindByIdNoIdFound() {
        Long nonExistentId = 999999999L;
        Mockito.when(giftCertificateRepository.findById(nonExistentId))
                .thenThrow(new DatabaseException("Item not found with id=" + nonExistentId));

        Assertions.assertThrows(ResponseStatusException.class, () -> giftCertificateService.findById(nonExistentId));
    }

    @Test
    void testSaveValidGift() {
        GiftCertificate addedGiftCertificate = completeGiftCertificate1;
        Mockito.when(giftCertificateRepository.save(addedGiftCertificate)).thenReturn(addedGiftCertificate);

        GiftCertificate result = giftCertificateService.save(addedGiftCertificate);
        Assertions.assertEquals(1L, result.getId());
        Assertions.assertEquals(30, result.getDuration());
        Assertions.assertEquals("Test Certificate 1", result.getName());
        Assertions.assertEquals(new BigDecimal("50.00"), result.getPrice());
        Assertions.assertEquals(creationDate, result.getCreateDate());
        Assertions.assertEquals(creationDate, result.getLastUpdateDate());
        Assertions.assertEquals(tagList1.size(), result.getTags().size());
    }

    @Test
    void testSaveIncompleteGift() {
        Assertions.assertThrows(ResponseStatusException.class, () -> giftCertificateService.save(incompleteGiftCertificate));
    }

    @Test
    void testUpdateValidGift() {
        GiftCertificate updateGiftCertificate = completeGiftCertificate1;
        Mockito.when(giftCertificateRepository.update(updateGiftCertificate)).thenReturn(updateGiftCertificate);
        Mockito.when(giftCertificateRepository.findById(updateGiftCertificate.getId())).thenReturn(updateGiftCertificate);

        GiftCertificate result = giftCertificateService.update(updateGiftCertificate);
        Assertions.assertEquals(1L, result.getId());
        Assertions.assertEquals(30, result.getDuration());
        Assertions.assertEquals("Test Certificate 1", result.getName());
        Assertions.assertEquals(new BigDecimal("50.00"), result.getPrice());
        Assertions.assertEquals(creationDate, result.getCreateDate());
        Assertions.assertEquals(tagList1.size(), result.getTags().size());
    }

    @Test
    void testUpdateInvalidGiftNoId() {
        Assertions.assertThrows(ResponseStatusException.class,
                () -> giftCertificateService.update(noIdGiftCertificate));
    }

    @Test
    void testUpdateInvalidGiftNoArguments() {
        Assertions.assertThrows(ResponseStatusException.class,
                () -> giftCertificateService.update(idOnlyGiftCertificate));
    }

    @Test
    void getTestDeleteByIdValid() {
        Long id = 1L;
        Mockito.doNothing().when(giftCertificateRepository).deleteById(id);

        giftCertificateService.deleteById(id);

        Mockito.verify(giftCertificateRepository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void testDeleteByIdInvalid() {
        Long id = 1L;
        Mockito.doThrow(new DatabaseException("No entry to delete, item id=" + id))
                .when(giftCertificateRepository).deleteById(id);

        Assertions.assertThrows(ResponseStatusException.class, () -> giftCertificateService.deleteById(id));

        Mockito.verify(giftCertificateRepository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void testGetSortedCertificatesValidInput() {
        String tagName = "tag1";
        String searchText = "Test";
        String sortBy = "name";
        String order = "asc";

        Mockito.when(giftCertificateRepository.getSortedCertificates(tagName, searchText, sortBy, order))
                .thenReturn(giftCertificates);

        List<GiftCertificate> result = giftCertificateService.getSortedCertificates(tagName, searchText, sortBy, order);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(2, result.size());
    }

    @Test
    void testGetSortedCertificatesInvalidSortBy() {
        String tagName = "tag1";
        String searchText = "Test";
        String sortBy = "invalid!sortBy";
        String order = "asc";

        Assertions.assertThrows(IllegalArgumentException.class, () ->
                giftCertificateService.getSortedCertificates(tagName, searchText, sortBy, order)
        );
    }

    @Test
    void testGetSortedCertificatesInvalidOrder() {
        String tagName = "tag1";
        String searchText = "Test";
        String sortBy = "name";
        String order = "invalidOrder";

        Assertions.assertThrows(IllegalArgumentException.class, () ->
                giftCertificateService.getSortedCertificates(tagName, searchText, sortBy, order)
        );
    }

}
