package com.epam.esm.service;

import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.Tag;
import com.epam.esm.repository.TagRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TagServiceTest {
    @Mock
    private TagRepositoryImpl tagRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    private Tag validTag1;
    private Tag validTag2;
    private Tag invalidTag;

    private List<Tag> tagList;

    @BeforeEach
    public void setUp() {
        validTag1 = new Tag();
        validTag1.setId(1L);
        validTag1.setName("Valid Tag 1");

        validTag2 = new Tag();
        validTag2.setId(2L);
        validTag2.setName("Valid Tag 2");

        invalidTag = new Tag();

        tagList = new ArrayList<>();
        tagList.add(validTag1);
        tagList.add(validTag2);
    }


    @Test
    void testFindAll() {
        Mockito.when(tagRepository.findAll()).thenReturn(tagList);

        List<Tag> result = tagService.findAll();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(2, result.size());
    }

    @Test
    void testFindAllEmpty() {
        Mockito.when(tagRepository.findAll()).thenReturn(new ArrayList<>());

        List<Tag> result = tagService.findAll();

        Assertions.assertNotNull(result);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    void testFindByIdValidTag() {
        Tag searchTag = validTag1;
        Mockito.when(tagRepository.findById(searchTag.getId())).thenReturn(validTag1);

        Tag result = tagService.findById(searchTag.getId());

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1L, result.getId());
        Assertions.assertEquals("Valid Tag 1", result.getName());
    }

    @Test
    void testFindByIdNoIdFound() {
        Long nonExistentId = 9999999L;
        Mockito.when(tagRepository.findById(nonExistentId))
                .thenThrow(new DatabaseException("Item not found with id=" + nonExistentId));

        Assertions.assertThrows(DatabaseException.class, () -> tagRepository.findById(nonExistentId));
    }

    @Test
    void testSaveValidTag() {
        Tag tagToAdd = validTag1;
        Mockito.when(tagRepository.save(tagToAdd)).thenReturn(tagToAdd);

        Tag result = tagService.save(tagToAdd);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(1L, result.getId());
        Assertions.assertEquals("Valid Tag 1", result.getName());
    }

    @Test
    void testSaveInvalidTag() {
        Assertions.assertThrows(ResponseStatusException.class,
                () -> tagService.save(invalidTag));
    }

    @Test
    void getTestDeleteByIdValid() {
        Long id = 1L;
        Mockito.doNothing().when(tagRepository).deleteById(id);

        tagService.deleteById(id);

        Mockito.verify(tagRepository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void testDeleteByIdInvalid() {
        Long id = 1L;
        Mockito.doThrow(new DatabaseException("No entry to delete, item id=" + id))
                .when(tagRepository).deleteById(id);

        Assertions.assertThrows(ResponseStatusException.class, () -> tagService.deleteById(id));

        Mockito.verify(tagRepository, Mockito.times(1)).deleteById(id);
    }


}
