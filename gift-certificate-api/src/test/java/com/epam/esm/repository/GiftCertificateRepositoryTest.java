package com.epam.esm.repository;


//import com.epam.esm.config.AppConfig;
import com.epam.esm.config.TestDatabaseConfig;
import com.epam.esm.config.WebConfig;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WebConfig.class, TestDatabaseConfig.class})
public class GiftCertificateRepositoryTest {

    @Autowired
    private GiftCertificateRepositoryImpl giftCertificateRepository;
    @Autowired
    private DataSource dataSource;

    @BeforeEach
    public void resetDatabase() {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
        resourceDatabasePopulator.addScript(new ClassPathResource("reset.sql"));
        resourceDatabasePopulator.execute(dataSource);
    }


    @Test
    void findAll() {
        List<GiftCertificate> expectedGiftCerts = TestRepositoryHelper.getExpectedGiftCertificates();
        List<GiftCertificate> returnedGiftCerts = giftCertificateRepository.findAll();

        Assertions.assertNotNull(returnedGiftCerts);
        Assertions.assertFalse(returnedGiftCerts.isEmpty());
        Assertions.assertEquals(expectedGiftCerts.size(), returnedGiftCerts.size());
    }

    @Test
    public void findGiftCertByIdValid() throws Exception {
        GiftCertificate expectedGiftCert = TestRepositoryHelper.getExpectedGiftCertificates().get(0);
        GiftCertificate returnedGiftCert = giftCertificateRepository.findById(expectedGiftCert.getId());

        Assertions.assertEquals(expectedGiftCert.getId(), returnedGiftCert.getId());
        Assertions.assertEquals(expectedGiftCert.getName(), returnedGiftCert.getName());
    }

    @Test
    public void findGiftCertByIdInvalid() throws Exception {
        Long nonExistentId = 999999L;
        DatabaseException exception = Assertions.assertThrows(DatabaseException.class,
                () -> giftCertificateRepository.findById(nonExistentId));
        Assertions.assertEquals("Item not found with id=" + nonExistentId, exception.getMessage());
    }

    @Test
    public void saveGiftCertValid() {
        GiftCertificate giftCertToStore = new GiftCertificate();
        giftCertToStore.setName("This is a valid Gift Cert");
        giftCertToStore.setDescription("Valid Description");
        giftCertToStore.setDuration(10);
        giftCertToStore.setPrice(BigDecimal.valueOf(99.99));

        List<Tag> giftTags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setName("Test Tag 1");
        giftTags.add(tag1);

        Tag tag2 = new Tag();
        tag2.setName("Test Tag 2");
        giftTags.add(tag2);

        giftCertToStore.setTags(giftTags);

        GiftCertificate savedGiftCert = giftCertificateRepository.save(giftCertToStore);

        Assertions.assertNotNull(savedGiftCert);
        Assertions.assertEquals(giftCertToStore.getName(), savedGiftCert.getName());
        Assertions.assertEquals(giftCertToStore.getDescription(), savedGiftCert.getDescription());
        Assertions.assertEquals(giftCertToStore.getDuration(), savedGiftCert.getDuration());
        Assertions.assertEquals(giftCertToStore.getPrice(), savedGiftCert.getPrice());
        Assertions.assertEquals(giftTags.size(), savedGiftCert.getTags().size());
    }

    @Test
    public void updateGiftCertValid() {
        GiftCertificate giftCertificateToUpdate = giftCertificateRepository.findById(1L);
        giftCertificateToUpdate.setName("New name");
        giftCertificateToUpdate.setDescription("New Description");
        giftCertificateToUpdate.setTags(new ArrayList<>());
        giftCertificateToUpdate.setDuration(1000);
        giftCertificateToUpdate.setPrice(BigDecimal.valueOf(29.54));

        GiftCertificate updatedGiftCertificate = giftCertificateRepository.update(giftCertificateToUpdate);

        Assertions.assertNotNull(updatedGiftCertificate);
        Assertions.assertEquals(updatedGiftCertificate.getId(), giftCertificateToUpdate.getId());
        Assertions.assertEquals(updatedGiftCertificate.getName(), giftCertificateToUpdate.getName());
        Assertions.assertEquals(updatedGiftCertificate.getDescription(), giftCertificateToUpdate.getDescription());
        Assertions.assertEquals(updatedGiftCertificate.getDuration(), giftCertificateToUpdate.getDuration());
        Assertions.assertEquals(updatedGiftCertificate.getPrice(), giftCertificateToUpdate.getPrice());
        Assertions.assertEquals(updatedGiftCertificate.getTags().size(), giftCertificateToUpdate.getTags().size());
    }

    @Test
    public void deleteGiftCertByIdValidId() {
        Long validId = 1L;
        List<GiftCertificate> giftCertsBeforeDelete = giftCertificateRepository.findAll();
        giftCertificateRepository.deleteById(validId);
        List<GiftCertificate> giftCertsAfterDelete = giftCertificateRepository.findAll();

        Assertions.assertEquals(giftCertsBeforeDelete.size(), giftCertsAfterDelete.size() + 1);
        Assertions.assertTrue(giftCertsBeforeDelete.stream()
                .anyMatch(giftCertificate -> giftCertificate.getId().equals(validId)));
        Assertions.assertFalse(giftCertsAfterDelete.stream()
                .anyMatch(giftCertificate -> giftCertificate.getId().equals(validId)));
    }

    @Test
    public void deleteGiftCertByIdInvalidId() {
        Long nonExistentId = 999999999L;

        DatabaseException exception = Assertions.assertThrows(DatabaseException.class,
                () -> giftCertificateRepository.deleteById(nonExistentId));
        Assertions.assertEquals("No entry to delete, item id=" + nonExistentId, exception.getMessage());
    }


    @Test
    public void getSortedCertificatesValid() {
        String tagName = "Test Tag 1";
        String searchText = "Test";
        String sortBy = "name";
        String sortDirection = "desc";

        List<GiftCertificate> returnedGiftCerts = giftCertificateRepository.getSortedCertificates(tagName, searchText, sortBy, sortDirection);

        Assertions.assertNotNull(returnedGiftCerts);
        Assertions.assertFalse(returnedGiftCerts.isEmpty());

        Assertions.assertTrue(returnedGiftCerts.get(0).getName().compareTo(returnedGiftCerts.get(1).getName()) >= 0);
    }


}
