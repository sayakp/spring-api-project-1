package com.epam.esm.repository;

import com.epam.esm.config.TestDatabaseConfig;
import com.epam.esm.config.WebConfig;
import com.epam.esm.exception.DatabaseException;
import com.epam.esm.model.Tag;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = {WebConfig.class, TestDatabaseConfig.class})
public class TagRepositoryTest {

    @Autowired
    private TagRepositoryImpl tagRepository;
    @Autowired
    private DataSource dataSource;


    @BeforeEach
    public void resetDatabase() {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
        resourceDatabasePopulator.addScript(new ClassPathResource("reset.sql"));
        resourceDatabasePopulator.execute(dataSource);
    }

    @Test
    void findAll() {
        List<Tag> expectedTags = TestRepositoryHelper.getExpectedTags();
        List<Tag> tags = tagRepository.findAll();

        Assertions.assertNotNull(tags);
        Assertions.assertFalse(tags.isEmpty());
        Assertions.assertEquals(expectedTags.size(), tags.size());
    }

    @Test
    public void findTagByIdValid() throws Exception {
        Tag expectedTag = TestRepositoryHelper.getExpectedTags().get(0);
        Tag tag = tagRepository.findById(expectedTag.getId());

        Assertions.assertEquals(expectedTag.getId(), tag.getId());
        Assertions.assertEquals(expectedTag.getName(), tag.getName());
    }

    @Test
    public void findTagByIdInvalid() throws Exception {
        Long nonExistentId = 999999L;
        DatabaseException exception = Assertions.assertThrows(DatabaseException.class,
                () -> tagRepository.findById(nonExistentId));
        Assertions.assertEquals("Item not found with id=" + nonExistentId, exception.getMessage());
    }

    @Test
    public void saveTagValid() {
        Tag tagToStore = new Tag();
        tagToStore.setName("This is a valid tag");

        Tag savedTag = tagRepository.save(tagToStore);

        Assertions.assertNotNull(savedTag);
        Assertions.assertEquals(tagToStore.getName(), savedTag.getName());
    }

    @Test
    public void deleteTagByIdValidId() {
        Long validId = 1L;
        List<Tag> tagsBeforeDelete = tagRepository.findAll();
        tagRepository.deleteById(validId);
        List<Tag> tagsAfterDelete = tagRepository.findAll();

        Assertions.assertEquals(tagsBeforeDelete.size(), tagsAfterDelete.size() + 1);
        Assertions.assertTrue(tagsBeforeDelete.stream().anyMatch(tag -> tag.getId().equals(validId)));
        Assertions.assertFalse(tagsAfterDelete.stream().anyMatch(tag -> tag.getId().equals(validId)));
    }

    @Test
    public void deleteTagByIdInvalidId() {
        Long nonExistentId = 999999999L;

        DatabaseException exception = Assertions.assertThrows(DatabaseException.class,
                () -> tagRepository.deleteById(nonExistentId));
        Assertions.assertEquals("No entry to delete, item id=" + nonExistentId, exception.getMessage());
    }

}
