package com.epam.esm.repository;

import com.epam.esm.model.GiftCertificate;
import com.epam.esm.model.Tag;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestRepositoryHelper {
    public static List<Tag> getExpectedTags() {
        List<Tag> expectedTags = new ArrayList<>();

        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Test Tag 1");
        expectedTags.add(tag1);

        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Test Tag 2");
        expectedTags.add(tag2);

        Tag tag3 = new Tag();
        tag3.setId(3L);
        tag3.setName("Test Tag 3");
        expectedTags.add(tag3);

        return expectedTags;
    }

    public static List<GiftCertificate> getExpectedGiftCertificates() {
        List<GiftCertificate> expectedGiftCertificates = new ArrayList<>();

        GiftCertificate giftCertificate1 = new GiftCertificate();
        giftCertificate1.setId(1L);
        giftCertificate1.setName("Test Gift Certificate 1");
        giftCertificate1.setDescription("Test Description 1");
        giftCertificate1.setPrice(BigDecimal.valueOf(10.99));
        giftCertificate1.setDuration(30);
        giftCertificate1.setCreateDate(new Date());
        giftCertificate1.setLastUpdateDate(new Date());
        List<Tag> gift1Tags = new ArrayList<>();
        gift1Tags.add(getExpectedTags().get(0));
        gift1Tags.add(getExpectedTags().get(1));
        giftCertificate1.setTags(gift1Tags);
        expectedGiftCertificates.add(giftCertificate1);

        GiftCertificate giftCertificate2 = new GiftCertificate();
        giftCertificate2.setId(2L);
        giftCertificate2.setName("Test Gift Certificate 2");
        giftCertificate2.setDescription("Test Description 2");
        giftCertificate2.setPrice(BigDecimal.valueOf(15.99));
        giftCertificate2.setDuration(45);
        giftCertificate2.setCreateDate(new Date());
        giftCertificate2.setLastUpdateDate(new Date());
        List<Tag> gift2Tags = new ArrayList<>();
        gift2Tags.add(getExpectedTags().get(1));
        gift2Tags.add(getExpectedTags().get(2));
        giftCertificate2.setTags(gift2Tags);
        expectedGiftCertificates.add(giftCertificate2);

        GiftCertificate giftCertificate3 = new GiftCertificate();
        giftCertificate3.setId(3L);
        giftCertificate3.setName("Test Gift Certificate 3");
        giftCertificate3.setDescription("Test Description 3");
        giftCertificate3.setPrice(BigDecimal.valueOf(20.99));
        giftCertificate3.setDuration(60);
        giftCertificate3.setCreateDate(new Date());
        giftCertificate3.setLastUpdateDate(new Date());
        List<Tag> gift3Tags = new ArrayList<>();
        gift3Tags.add(getExpectedTags().get(0));
        gift3Tags.add(getExpectedTags().get(2));
        giftCertificate3.setTags(gift3Tags);
        expectedGiftCertificates.add(giftCertificate3);

        return expectedGiftCertificates;
    }
}
