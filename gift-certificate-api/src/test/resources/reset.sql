-- Delete existing data from gift_certificate_tag table
DELETE
FROM gift_certificate_tag;


-- Delete existing data from gift_certificate and tag tables
DELETE
FROM gift_certificate;
DELETE
FROM tag;


-- Insert sample gift_certificate data
INSERT INTO gift_certificate (id, name, description, price, duration)
VALUES (1, 'Test Gift Certificate 1', 'Test Description 1', 10.99, 30),
       (2, 'Test Gift Certificate 2', 'Test Description 2', 15.99, 45),
       (3, 'Test Gift Certificate 3', 'Test Description 3', 20.99, 60);

-- Insert sample tag data
INSERT INTO tag (id, name)
VALUES (1, 'Test Tag 1'),
       (2, 'Test Tag 2'),
       (3, 'Test Tag 3');

-- Insert sample gift_certificate_tag data
INSERT INTO gift_certificate_tag (gift_certificate_id, tag_id)
VALUES (1, 1),
       (1, 2),
       (2, 2),
       (3, 1),
       (3, 3);

-- Set the auto-increment counter for each table for H2 to keep track
ALTER TABLE gift_certificate
    ALTER COLUMN id RESTART WITH 4;
ALTER TABLE tag
    ALTER COLUMN id RESTART WITH 4;