CREATE TABLE gift_certificate
(
    id               SERIAL PRIMARY KEY,
    name             VARCHAR(255)   NOT NULL NOT NULL,
    description      TEXT,
    price            NUMERIC(12, 2) NOT NULL,
    duration         INTEGER        NOT NULL,
    create_date      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    last_update_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE tag
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE gift_certificate_tag
(
    gift_certificate_id INTEGER REFERENCES gift_certificate (id),
    tag_id              INTEGER REFERENCES tag (id),
    PRIMARY KEY (gift_certificate_id, tag_id)
);